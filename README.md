### Client eimzo

> Install with command:

```bash
yarn add @shohrux_saidov/eimzo-client
```

or

```bash
npm i @shohrux_saidov/eimzo-client
```

```js

initModuleEimzo().then(async eimzo => {
    await eimzo.checkVersion()
    await eimzo.installApiKeys()
    eimzo.addKey('test.com', 'key') // key from soliq
    const keys = await res.listAllUserKeys()
})
```

#### Methods

> - checkVersion (required to invoke immediately async function)
> - installApiKeys (required to invoke immediately async function)
> - addKey
> - removeKey
> - listAllUserKeys
> - getHashESign
> - loadKey
> - getTimestampToken
> - signPkcs7
> 