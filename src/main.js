import {initModuleEimzo} from './index.js'

initModuleEimzo().then(async res => {
    await res.checkVersion()
    await res.installApiKeys()
   const data =  await res.listAllUserKeys()
    console.log(data)
})