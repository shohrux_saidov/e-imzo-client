export interface ESignKey {
    name: string
    alias: string
    serialNumber: string
    CN:string
    O: string
    PINFL:string
    T:string
    TIN:string
    UID: string
    validFrom: Date
    validTo: Date
    path: string
}

interface EsignModule {
    checkVersion: () => any;
    listAllUserKeys: () => Promise<ESignKey[]>;
    getKeys: () => string[];
    addKey: (domain: string, key: string) => void;
    removeKey: (domain: string) => void;
    installApiKeys: () => any;
    loadKey: (cert: any) => any;
    signPkcs7: (cert: ESignKey, content: any, id?: any) => any;
    getTimestampToken: (signature: any) => Promise<any>;
    getHashESign: (cert: ESignKey, content: any, id?: any) => any;
}

export function initModuleEimzo(): Promise<EsignModule>;


