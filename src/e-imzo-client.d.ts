export namespace EIMZOClient {
    const NEW_API: boolean;
    const API_KEYS: string[];
    function checkVersion(success: any, fail: any): void;
    function installApiKeys(success: any, fail: any): void;
    function listAllUserKeys(itemIdGen: any, itemUiGen: any, success: any, fail: any): void;
    function loadKey(itemObject: any, success: any, fail: any): void;
    function changeKeyPassword(itemObject: any, success: any, fail: any): void;
    function createPkcs7(id: any, data: any, timestamper: any, success: any, fail: any): void;
    function _getX500Val(s: any, f: any): any;
    function _findPfxs2(itemIdGen: any, itemUiGen: any, items: any, errors: any, callback: any): void;
    function _findTokens2(itemIdGen: any, itemUiGen: any, items: any, errors: any, callback: any): void;
}
