/**
 * @typedef {
 * Promise<{
 * getHashESign?: function(*, *): Promise<unknown>,
 * listAllUserKeys?: function(): Promise<unknown>,
 * loadKey?: function(*): Promise<unknown>,
 * installApiKeys?: function(): Promise<unknown>,
 * getTimestampToken?: function(*): Promise<unknown>,
 * signPkcs7?: function(*, *): Promise<unknown>,
 * getKeys: function(): string[],
 * addKey: function(domain:string, key:string): void,
 * removeKey: function(domain:string): void,
 * checkVersion?: function(): Promise<unknown>}>} Module
 */

/**
 *
 * @returns {Module}
 */
export const initModuleEimzo = async () => {
  return await import('./EIMZO.js')
}
