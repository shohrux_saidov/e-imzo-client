import './e-imzo.js'
import {EIMZOClient} from './e-imzo-client.js'

const errors = {
    CERT_REQUIRED: 'CERT_REQUIRED',
    INVALID_PASSWORD: 'INVALID PASSWORD'
}

export const checkVersion = () => {
    return new Promise((resolve, reject) => {
        EIMZOClient.checkVersion(resolve, reject)
    })
}


export const listAllUserKeys = () => {
    return new Promise((resolve, reject) => {
        EIMZOClient.listAllUserKeys(
            function (cert,) {
                return cert
            }, function (index, cert) {
                return cert
            },
            function (items, firstId) {
                resolve(items, firstId)
            },
            function (error, reason) {
                reject({error, reason})
            }
        )
    })
}

/**
 * @returns {string[]}
 */
export const getKeys = () => {
    return EIMZOClient.API_KEYS
}

/**
 *
 * @param domain {string}
 * @param key {string}
 */
export const addKey = (domain, key) => {
    EIMZOClient.API_KEYS.push(domain, key)
}

/**
 *
 * @param domain {string}
 */
export const removeKey = (domain) => {
    const domainIndex = EIMZOClient.API_KEYS.indexOf(domain)
    if (domainIndex === -1) return
    EIMZOClient.API_KEYS.splice(domainIndex, 2)
}

export const installApiKeys = () => {
    return new Promise((resolve, reject) => {
            EIMZOClient.installApiKeys(function (e, data) {
                    resolve()
                },
                function (err, reason) {
                    reject({err, reason})
                })
        },
    )
}


export const loadKey = (cert) => {
    return new Promise((resolve, reject) => {
        if (!cert) reject(errors.CERT_REQUIRED)
        EIMZOClient.loadKey(
            cert,
            (id) => {
                resolve({id, cert})
            },
            function () {
                reject(errors.INVALID_PASSWORD)
            }
        )
    })
}

export const signPkcs7 = (cert, content, id = null) => {
    return new Promise(async (resolve, reject) => {
        if (!id) {
            try {
                let loadKeyResult = await loadKey(cert)
                id = loadKeyResult.id
            } catch (e) {
                reject(e)
            }
        }
        CAPIWS.callFunction({
            name: 'create_pkcs7', plugin: 'pkcs7', arguments: [
                window.Base64.encode(content), id, 'no'
            ]
        }, (event, data) => {
            if (data.success) {
                resolve({hash: data, id})
            } else {
                reject('Failed')
            }
        }, reject)
    })
}

export const getTimestampToken = async (signature) => {
    return new Promise((resolve, reject) => {
        CAPIWS.callFunction({
            name: 'get_timestamp_token_request_for_signature',
            arguments: [signature]
        }, function (event, data) {
            if (data.success) {
                resolve(data.timestamp_request_64)
            } else {
                reject('Failed')
            }
        }, reject)
    })
}

export const getHashESign = (cert, content, id = null) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await signPkcs7(cert, content, id)
            let token = await getTimestampToken(result.hash.signature_hex)
            resolve({token, id: result.id, hash: result.hash.pkcs7_64})
        } catch (e) {
            reject(e)
        }
    })
}
